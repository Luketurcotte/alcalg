\documentclass[11pt]{article}
\usepackage[authoryear,round,longnamesfirst]{natbib}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{microtype}
\usepackage{todonotes}
\usepackage[nolist,nohyperlinks]{acronym}
\usepackage{parskip}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{setspace}
\usepackage{indentfirst}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{array}
\newcommand*{\MyIndent}{\hspace*{0.5cm}}
\usepackage{tabu}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{threeparttable}
\usepackage[fleqn]{amsmath}
\setlength{\parindent}{1cm}
\usepackage[section,subsection,subsubsection]{extraplaceins}
\usepackage{float}
\usepackage[small]{titlesec}
%\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\usepackage{enumitem}
%\usepackage{palatino}
\hyphenpenalty=1000
\exhyphenpenalty=1000
\usepackage[autostyle, english = american]{csquotes}
\MakeOuterQuote{"}
\usepackage{tikz}
\usepackage{forest}
\usepackage{adjustbox}
\usepackage{pdflscape}

\definecolor{one}{RGB}{51,0,65}
\definecolor{two}{RGB}{49,49,115}
\definecolor{three}{RGB}{34,100,122}
\definecolor{four}{RGB}{32,153,113}
\definecolor{five}{RGB}{105,203,65}
\definecolor{six}{RGB}{252,227,30}

\def\tabularxcolumn#1{b{#1}}
\newcolumntype{C}[1]{>{\centering}P{#1}}
\begin{document}

<<load, echo=FALSE, include=FALSE>>=
require(plot)
require(knits)
@


\input{./titlepage}

\section{Introduction}

Patient flow describes the operational manner by which patients transition between care settings along the continuum of care \citep{Cote:2000, Marshall:2005jl}. For older adults with complex health and social needs, admission to an acute care hospital may only represent a portion of the care pathway which may extend to post-acute and long-term residential or community based care \citep{Victor:2000}. Delays in the movement of patients to adjacent care settings along the continuum of care are an inefficient use of system resources \citep{Lenzi:2014} and a reflection of health system quality \citep{Costa:2012}. In comparison to acute care hospitals, which often serve as the health system entry point for an episode of care \citep{Hirshon:2013}, patient flow is understudied in post-acute care settings \citep{New:2013cq}. In spite of this, barriers to admission and discharge in post-acute care settings have the potential to impede flow across a health system \citep{New:2013cq, New:2014, Bradley:2006,Worthington:2006sf}. 

%TODO insert a sentence or two about the negative outcomes for patients experiencing delayed dsischarge. Look to sutherland for an example. 

%Though length of stay is used as a indicator of patient flow \citep{Marshall:2005jl, New:2013mb}, delayed discharge from hospital may occur independently of prolonged length of stay. Distinguishing patients that may only transition with additional supports from those that will require prolonged or indefinite care as a result chronic conditions or clinical complexity is necessary to identify patients at risk of delayed discharge \citep{Gassoumis:2013dp}. 

Previous studies have identified numerous social, organizational, and individual factors associated with delayed hospital discharge. Access to services along the continuum of care including rehabilitative \citep{Lenzi:2014}, residential \citep{Victor:2000, Challis:2013uq, Lenzi:2014, Tan:2010by, Costa:2012, New:2014}, and community care \citep{Victor:2000, Challis:2013uq} are commonly associated with delayed hospital discharge. Though rates of delayed discharge are sensitive to increased supply \citep{Gaughan:2015}, barriers to discharge are often systematic in nature and may manifest in the form delays in obtaining approval for access \citep{New:2014} and payment \citep{Worthington:2006sf}. Social isolation \citep{Landeiro:2015}, living alone, \citep{Victor:2000}, the availability of informal caregivers \citep{Victor:2000, Lenzi:2014}, need for home modifications \citep{New:2014, New:2013cq}, and family deliberations in discharge planning \citep{New:2014, New:2013cq} may also delay discharge.


With respect to individual factors, male gender is associated with delayed hospital discharge \citep{Costa:2012, New:2013cq}; however, there is some disagreement with regards to the effect of age \citep{Costa:2012, New:2013cq, New:2014, Challis:2013uq}. Associated clinical conditions include stroke \citep{Costa:2012, New:2013cq}, dementia \citep{Lenzi:2014}, fracture \citep{Lenzi:2014, New:2014}, traumatic brain injury \citep{New:2013cq}, and psychiatric diagnosis \citep{Costa:2012, Challis:2013uq}. Patients admitted with cognitive impairment \citep{Challis:2013uq, Costa:2012}, functional impairment \citep{Worthington:2006sf, Tan:2010by, New:2014, New:2013cq}, and behavioural issues \citep{Costa:2012, Challis:2013uq} are more likely to experience delayed hospital discharge.


There are few decision-support tools available to assist discharge planners in identifying patients at risk of delayed discharge in post-acute care. The Blaylock Risk Assessment Screening Score (BRASS) is an index-based tool designed to identify hospital patients at risk of prolonged hospital stay and difficulties after discharge \citep{Mistiaen:1999aa}. As it was developed for use in an acute care setting, when applied in a post-acute rehabilitation setting it demonstrates poor reliability, limiting its utility for identifying patients likely to experience post-discharge difficulties \citep{Panella:2012}. 

In Ontario, Canada, Complex Continuing Care (CCC) facilities provide hospital based nursing and rehabilitation care following acute hospitalization \citep{OHACCC:2006hl}, and serve as a transition point to community and residential care settings. As an intermediary in the health system, CCC facilities are often subject to patient flow pressures, as 21\% of CCC beds are occupied by Alternate Level of Care (ALC) patients \citep{CCO:2016} who no longer require the intensity of resources and services that are provided in this setting and are waiting for discharge to a more appropriate care setting \citep{CCO:2009}. Seventy-one percent of patients designated as ALC in Ontario CCC facilities wait 30 or more days for discharge and are considered to be long-stay ALC patients \citep{Turcotte:2015}. 

This study describes the development of the Post-acute Delayed Discharge Risk Scale (PADDRS), a new outcome measure designed to classify patients by risk of delayed discharge upon admission to a post-acute care setting. Using ALC patients in Ontario CCC hospitals as a delayed discharge outcome, this study validates PADDRS as a tool with clinical applications in discharge planning and risk-adjusted quality measurement. 

\section{Methods}

\subsection{Population and Study Design}

This was a retrospective cohort study of interRAI MDS 2.0 comprehensive health assessments from the Canadian Institute of Health Information (CIHI) Continuing Care Reporting System (CCRS) linked to Cancer Care Ontario (CCO) Wait Time Information System (WTIS) records.

The MDS 2.0 is a comprehensive clinical assessment used in continuing care settings to evaluate patients across a broad range of health domains including physical functioning, cognition, mood and behaviour, social functioning, diseases and conditions, health service and medication utilization \citep{Bernabei:2008lq, Gray:2009kt, Hirdes:2008uq, Ikegami:2002vn}. All Ontario CCC patients, except short-stay patients with a length of stay less than 14 day, are assessed with the MDS 2.0 instrument.

Wait times across a variety of service settings in Ontario are reported through the WTIS. This system includes patient level data on wait times (ALC days), basic demographics, wait time service setting, most appropriate discharge destination and the need for specialized supports required at the discharge destination\citep{CCO:2015c}. These data provide a means of measuring patient flow through the Ontario health system; though, they lack the necessary clinical information to estimate the effect of individual patient attributes on flow. 

All patients admitted to Ontario CCC facilities and assessed using the MDS 2.0 between January 1st, 2011 and March 31st, 2013 were included in the study with the exception of patients admitted to fifteen facilities that did not submit WTIS records during this time interval. A total sample of 30,966 admission assessments from 84 facilities were included in the study.  

\subsection{Outcome Measurement}

The primary dependent variable of delayed post-acute discharge for this study was long-stay ALC designation, defined as thirty or more ALC days accumulated during an episode of care in an Ontario CCC facility. Episodes of care were defined as a continuous stay in a CCC facility; though, a single interruption in care of less than 14 days during the episode of care was permissible. Measurement of ALC wait time began on the day of entry for all patients. Right censoring of discharge dates for patients that had not yet been discharged by March 31st, 2013 was performed. For censored patients designated as ALC prior to March 31st, 2013, the censored discharge date was used in the calculation of total ALC wait time.    

\subsection{Independent Variables}

Independent variables used in this study were clinical items, outcome measures, and summary scales from the MDS 2.0 admission assessment. Clinical items included information on patient demographics, residential history, indicators of psychosocial well-being, discharge potential, recent health service use, diseases and other health conditions. Collapsed versions of the ADL Self-Performance Hierarchy Scale (ADL-H), Cognitive Performance Scale (CPS), Changes in Health, End-Stage Disease, Signs and Symptoms Scale (CHESS), Aggressive Behaviour Scale (ABS), and Index of Social Engagement (ISE) were used. 

The ADL-H scale measures performance in early and late loss activities of daily living to create a hierarchical measure of functional capacity \citep{Williams:1997rz}. ADL-H Scale scores range from 0 to 6. CPS is a measure of cognitive function and groups patients and ranges from 0 to 6 \citep{Morris:1994ve}. CPS has been validated against the Mini-Mental State Exam (MMSE) \citep{Hartmaier:1995fq}. CHESS is a measure of health instability that is predictive of mortality among institutionalized older adults \citep{Hirdes:2003} and persons with neurological conditions\citep{Hirdes:2014gb}. CHESS scores range between 0 and 5. The ABS is a measure of aggressive behaviour that takes into account the frequency of verbal abuse, physical abuse, socially disruptive behaviour, and resistance to care \citep{Perlman:2008rw}. It has been validated against the aggression sub-scale of the Cohen Mansfield Agitation Inventory (CMAI) \citep{Perlman:2008rw}. ABS scores range between 0 and 12. PURS is a seven item scale that it is used to identifying patients at greatest risk of developing pressure ulcers \citep{Poss:2010qe}. Finally, the Index of Social Engagement (ISE) summarizes a patient's social behaviour through engagement and participation in social opportunities offered by the facility \citep{Mor:1995ta,Gerritsen:2008sp}. ISE scores range from 0 to 6. 
\subsection{Scale Derivation}

A multivariable logistic regression model was previously created to characterize the associations between independent variables and long-stay ALC designation for Ontario CCC patients \citep{Turcotte:2015}. This model was used to identify candidate variables to be used in the derivation of PADDRS using a classification tree to group patients by likelihood of delayed discharge using patient attributes measures on admission to post-acute care. In order to construct the classification tree, the sample of Ontario CCC patients was randomly divided into training (70\%) and validation (30\%) datasets. Root and internal node classifiers were selected based on the magnitude of likelihood ratio test statistic and common clinical knowledge. In general, items describing clinical or social characteristics were selected in favour of process variables (i.e., provision of treatments and therapies) that may vary based on individual facility or regional practice patterns. Exceptions include provision of therapies such as suctioning or tracheostomy care as the MDS 2.0 assessment does not include other means of identifying patients with conditions that would be benefit from these types of therapies. Following construction of the classification tree, terminal nodes with similar proportions of long-stay ALC patients were grouped together to form the PADDRS delayed discharge risk strata. Construction of the PADDRS classification tree was conducted using SAS Enterprise Miner 6.2 (SAS Institute, Inc., Cary, NC). 

\subsection{Descriptive Statistics and Scale Validation}

Descriptive statistic comparing sociodemographic factors, clinical status, and the availability of social supports for Ontario CCC patients experiencing timely and delayed post-acute care discharge were computed. Frequencies distributions were compared using the Pearson chi-square $({\chi}^2)$ test. 

Validation of PADDRS as a tool for predicting delayed post-acute discharge was performed through the computation of class-level effect sizes and a receiver operating characteristic (ROC) curve. Jurisdictional sensitivity analyses of model fit were performed by comparing area under the curve (AUC) statistics for each of Ontario's Local Health Integration Networks (LHINs), which divide the province geographically into fourteen independent health regions. Analyses were performed using SAS 9.4 (SAS Institute, Inc., Cary, NC). 

\section{Results}

\subsection{Descriptive Statistics}

Overall, 10.3\% of the sample of the sample of 30,966 Ontario CCC patients experienced the delayed discharge outcome event, defined in this study as long-stay ALC designation. Table~\ref{tab:patientchars} compares patient characteristics for regular and delayed discharge patients. Among patients experiencing delayed discharge, a greater proportion lived alone and lacked personal contact with friends or family. A prior stay in board and care, psychiatric, or subacute care facilities in the preceding five years occurred more often for patients with delayed discharge. With regards to discharge potential, a lesser percentage of patients experiencing delayed had a desire to return to the community and had a support person who was positive towards discharge to the community, presumably these patients are waiting for discharge to a residential care setting such as a nursing home. Diseases diagnoses such as Alzheimer's disease and related dementias, depression, bipolar disease, schizophrenia, stroke and traumatic brain injury were more common among patients experiencing delayed discharge. A greater proportion of delayed discharge patients hallucinations or delusions, and bowel or bladder incontinence. 

\input{./tables/patientchars.tex}

Table~\ref{tab:scales} presents scale score distributions for regular and delayed discharge patients in the sample. A greater proportion of delayed discharge patients experienced moderate to severe functional and cognitive impairment, as determined by the ADL-H and CPS scales. More severe ABS scale scores indicates that delayed discharge patients exhibited an array of common aggressive behaviours more frequently. Fewer delayed discharge patients exhibited moderate or worse clinical instability, as determined by the CHESS. Finally, the score distribution for the ISE indicates that delayed discharge patients were less socially engaged on admission to the post-acute care facility.     

\input{./tables/scales.tex}

\subsection{Derivation}

The PADDRS classification tree groups patients into 29 distinct groups by likelihood of delayed discharge (Figure~\ref{fig:tree}). Cognitive skills for daily decision making, a measure of cognitive performance, had the greatest discriminatory power and served as the root node classifier in the tree. Subsequent internal node classifications aimed to differentiate patients with end-stage conditions or other clinically complex conditions from those requiring care that could be offered in a less intensive setting such as community or residential care. Examples of clinically complex patients include those requiring advanced medical treatments such as IV medication, oxygen therapy, suctioning, tracheostomy care, or nutrition through use of a feeding tube as they are more likely to be within a post-acute care facilities scope of practice and are therefore unlikely to be eligible to transition and experience delayed discharge \todo{insert citation about the scope of CCC care}. Patients with a greater number of acute care hospital admissions in the ninety days prior to admission to a post-acute care facility had greater odds of delayed discharge. The presence of social supports such as a "support person who is positive towards discharge" or a spouse act to reduce the likelihood of experiencing delayed discharge. Those that are eligible for discharge but "did not have a desire to return to the community" were presumably waiting for placement in a residential care setting. Finally, patients with greater bowel incontinence severity were classified into groups with a greater likelihood of delayed discharge, likely representing advanced functional impairment or physical care needs which might places considerable burden on informal caregivers. 

\begin{landscape}
 	\begin{figure}[htbp]
		\fbox{\includegraphics[width=0.9\linewidth,keepaspectratio]{../tree.pdf}}
			\caption{Post-acute Delayed Discharge Risk Scale (PADDRS)}
			\label{fig:tree}
	\end{figure}
\end{landscape}

\subsection{Validation}

The distribution of PADDRS scale scores is the sample of Ontario CCC patients is positively skewed, indicating only small proportion of all CCC patients are at the highest levels of risk of delayed discharge. Class-level odds ratio estimates for delayed post-acute discharge increased across the range of PADDRS scores (Table~\ref{tab:validity}). An overall AUC statistic of 0.73 demonstrates that the model has good fit when applied to the overall sample of Ontario CCC patients (Table~\ref{tab:validity_lhin}). Sensitivity analysis for model fit yielded AUC statistics ranging between 0.62 and 0.81 across Ontario's fourteen LHINs indicating that PADDRS is generalizable across geographic regions with varying approaches to patient flow. 
  
\input{./tables/validity.tex}
\input{./tables/validity_lhin.tex}


\section{Discussion}

PADDRS demonstrates that important clinical factors associated with delayed discharge in a post-acute care setting include cognitive impairment, functional impairment, and a lack of social supports to enable timely discharge. Previous studies support these findings \citep{Tan:2010by, New:2014, New:2013cq, Challis:2013uq, Lenzi:2014, Victor:2000}. In addition, past admission to a post-acute care hospital and an increased number of recent acute care hospitalizations are also associated with increased likelihood of delayed discharge. Though these later factors have not previously been reported as risk factors for delayed discharge in a post-acute care setting, they may be indicative of clinical frailty and increased likelihood of residential care admission \citep{Campitelli:2016}. PADDRS also identifies patients that are most likely to benefit from care provided in a post-acute care setting, such as those with clinically complex or end-stage conditions, as these patients are less likely to be transitioned to a less intensive setting along the continuum of care ~\citep{Wodchis:2004pd}. Validation of this new risk scale was accomplished using a sample of Ontario CCC patients experiencing long-stay ALC designation. Through this analysis, PADDRS has demonstrated sufficient discriminatory power to have utility in identifying patients at risk of delayed discharge from a post-acute care setting.


%Notably absent from the PADDRS classification tree are items related to aggressive behaviour. While some literature \cite{Challis:2013uq, Costa:2012} and a much of the public discourse surrounding patients experiencing delayed discharge is focused on the effect of aggressive behaviour, after accounting for the effect of cognitive impairment, aggression has little discriminatory power. Indeed, cognitive impairment is believed to be a confounding factor explaining the association between aggressive behaviour and delayed discharge.  

A primary motivation for this study was to develop a risk scale that would aid discharge planners in identifying patients at greatest risk of delayed discharge early in an episode of care. Doing so would provide a systematic means of prioritizing referrals to home and residential care for patients most likely to experience difficulty in transitioning to less intensive care settings when it is appropriate. Though there are a lack of patient flow intervention studies in post-acute care, there is some evidence to suggest that standardized discharge planning processes are beneficial \citep{Tan:2010by}. PADDRS supports system integration as a means of improving patient flow \cite{Sutherland:2013sw} through the health system as it provides a foundation for open and constructive discussions with partners along the continuum of care to address inter-organizational barriers to discharge \citep{Baumann:2007}. Given that PADDRS is designed as summary scale output from interRAI comprehensive health assessments completed on admission, the information that it provides discharge planners and other members of the care team is available without the need to introduce additional assessments that may contribute to assessment burden. 
 
PADDRS also enables health system administrators to perform risk-adjusted performance comparisons for measures of patient flow. For example, the maintainers of Ontario's Wait Time Information System distribute monthly "ALC Performance" summary reports that compare health jurisdictions by ALC rate \cite{CCO:2016}. These crude ALC rate statistics fail to account for patient case-mix, and thus comparisons between peers cannot be made equitably. Similar to the methodology used for other quality indicators based on interRAI data \cite{Jones:2010wq}, PADDRS may be used as the basis for risk stratification in the calculation of patient flow measures which may be used as quality indicators. This allows health regions or individual facilities to be justly compared and ensures that identified "top-performers" simply aren't those with the fewest patients at high risk of delayed discharge.

PADDRS was derived using the MDS 2.0 comprehensive health assessment and the items that it is based on are available on several other interRAI assessment systems including interRAI Long-term Care Facility (interRAI LTC) and Post-acute Care (interRAI PAC) assessment widely implemented outside of North America \citep{Bernabei:2008lq, Hirdes:2008uq}. Compatibility with these assessments allows PADDRS to be used in a variety of post-acute care settings to classify patients by risk of delayed discharge. Validation of PADDRS as a tool for predicting delayed discharge in other international jurisdictions and post-acute care contexts is necessary. Further, PADDRS is also compatible with the RAI Home Care (RAI-HC) assessment used in community care settings. Though additional validation work is necessary, it's conceivable that PADDRS will have utility in identifying patients at risk of post-acute delayed discharge prior to admission using data from a recent RAI-HC assessment. 

There are some limitations to the PADDRS scale that should be acknowledged. As a patient-level classification tool, PADDRS is unable to take into account local contexts such as service availability, policies, incentives, and system configuration. Although these are important factors that have been shown to affect patient flow \todo{insert citations}, accounting for these factors would require additional data inputs beyond the interRAI assessment system and would limit the generalizability of the tool. Similarly, given that PADDRS is reliant on variables present across interRAI assessment instruments, only basic descriptions of social supports are available on the assessments used to derive the scale. For example, while martial status is used as a classifier, the capacity of these informal caregivers to provide the support necessary to enable timely discharge may be not be determined using the assessment. Given that it is designed as a decision-support tool, discharge planners implementing PADDRS should augment its classification of delayed discharge risk with all other available contextual information. 

\section{Conclusion}

Delayed post-acute care discharge has the potential to impede flow across the entire health system and restrict access for other system users. PADDRS was developed in response to the identified need for a decision-support tool to aid post-acute care discharge planners in classifying patients by risk of delayed discharge. Its applications in discharge planning and risk-adjusted quality measurement are valuable to both front-line staff and health system administrators. 




		

  























































% The rest of the front pages should contain no headers and be numbered using Roman numerals starting with `ii'
\pagestyle{plain}
\setcounter{page}{2}

\cleardoublepage % Ends the current page and causes all figures and tables that have so far appeared in the input to be printed.



\cleardoublepage % This is needed if the book class is used, to place the anchor in the correct page,
                 % because the bibliography will start on its own page.
                 % Use \clearpage instead if the document class uses the "oneside" argument
\phantomsection  % With hyperref package, enables hyperlinking from the table of contents to bibliography             
% The following statement causes the title "References" to be used for the bibliography section:
\renewcommand*{\bibname}{References}

% Add the References to the Table of Contents
%\addcontentsline{toc}{chapter}{\textbf{References}}
\bibliography{20160718_ALCAlg_paper}{}
\bibliographystyle{apalike}

%\renewcommand*{\bibname}{References}
%
%% Add the References to the Table of Contents
%\addcontentsline{toc}{chapter}{\textbf{References}}
%\newpage
%\bibliography{bibdesk}{}
%\bibliographystyle{apalike}
\end{document}
