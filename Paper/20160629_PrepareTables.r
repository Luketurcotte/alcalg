library(reshape)
library(plyr)

history <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_history.csv", header=TRUE)

history_cast <- cast(history, history~alc)



history_cast <- rename(history_cast,c("history"="his","1"="One","0"="Zero"))

history_cast[] <- lapply(history_cast, as.character)

for (i in 1:nrow(history_cast)) {
	history_cast$Zero[i]<- paste(history_cast$Zero[i], " &", sep="")
	history_cast$One[i]<- paste(history_cast$One[i], " \\\\", sep="")	
	history_cast$his[i]<- paste(history_cast$his[i], " &", sep="")
}

write.table(history_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/history.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)



disease <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_diseases.csv", header=TRUE)

disease_cast <- cast(disease, disease~alc)



disease_cast <- rename(disease_cast,c("disease"="his","1"="One","0"="Zero"))

disease_cast[] <- lapply(disease_cast, as.character)

for (i in 1:nrow(disease_cast)) {
	disease_cast$Zero[i]<- paste(disease_cast$Zero[i], " &", sep="")
	disease_cast$One[i]<- paste(disease_cast$One[i], " \\\\", sep="")	
	disease_cast$his[i]<- paste(disease_cast$his[i], " &", sep="")
}

write.table(disease_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/disease.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

condition <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_conditions.csv", header=TRUE)

condition_cast <- cast(condition, condition~alc)



condition_cast <- rename(condition_cast,c("condition"="his","1"="One","0"="Zero"))

condition_cast[] <- lapply(condition_cast, as.character)

for (i in 1:nrow(condition_cast)) {
	condition_cast$Zero[i]<- paste(condition_cast$Zero[i], " &", sep="")
	condition_cast$One[i]<- paste(condition_cast$One[i], " \\\\", sep="")	
	condition_cast$his[i]<- paste(condition_cast$his[i], " &", sep="")
}

write.table(condition_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/condition.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)


adl <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_adl.csv", header=TRUE)
adl$overall <- format(adl$overall, digits=1, nsmall=1)
adl_cast <- cast(adl, adl~alc)



adl_cast <- rename(adl_cast,c("adl"="his","1"="One","0"="Zero"))

attach(adl_cast)
adl_cast$his[his ==0] <- "0"
adl_cast$his[his ==1] <- "1-2"
adl_cast $his[his ==2] <- "3-4"
adl_cast $his[his ==3] <- "5-6" 
detach(adl_cast)


adl_cast[] <- lapply(adl_cast, as.character)

for (i in 1:nrow(adl_cast)) {
	adl_cast$Zero[i]<- paste(adl_cast$Zero[i], " &", sep="")
	adl_cast$One[i]<- paste(adl_cast$One[i], " \\\\", sep="")	
	adl_cast$his[i]<- paste(adl_cast$his[i], " &", sep="")	
	adl_cast$his[i]<- paste0( "\\MyIndent ", adl_cast$his[i], sep="")
}

write.table(adl_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/adl.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

cps <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_cps.csv", header=TRUE)

cps$overall <- format(cps$overall, digits=1, nsmall=1)

cps_cast <- cast(cps, cps~alc)



cps_cast <- rename(cps_cast,c("cps"="his","1"="One","0"="Zero"))

attach(cps_cast)
cps_cast$his[his ==0] <- "0"
cps_cast$his[his ==1] <- "1-2"
cps_cast $his[his ==2] <- "3-4"
cps_cast $his[his ==3] <- "5-6" 
detach(cps_cast)



cps_cast[] <- lapply(cps_cast, as.character)

for (i in 1:nrow(cps_cast)) {
	cps_cast$Zero[i]<- paste(cps_cast$Zero[i], " &", sep="")
	cps_cast$One[i]<- paste(cps_cast$One[i], " \\\\", sep="")	
	cps_cast$his[i]<- paste(cps_cast$his[i], " &", sep="")	
	cps_cast$his[i]<- paste0( "\\MyIndent ", cps_cast$his[i], sep="")
}

write.table(cps_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/cps.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

abs <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_abs.csv", header=TRUE)

abs$overall <- format(abs$overall, digits=1, nsmall=1)

abs_cast <- cast(abs, abs~alc)



abs_cast <- rename(abs_cast,c("abs"="his","1"="One","0"="Zero"))

attach(abs_cast)
abs_cast$his[his ==0] <- "0"
abs_cast$his[his ==1] <- "1-4"
abs_cast$his[his ==2] <- "5+"
detach(abs_cast)



abs_cast[] <- lapply(abs_cast, as.character)

for (i in 1:nrow(abs_cast)) {
	abs_cast$Zero[i]<- paste(abs_cast$Zero[i], " &", sep="")
	abs_cast$One[i]<- paste(abs_cast$One[i], " \\\\", sep="")	
	abs_cast$his[i]<- paste(abs_cast$his[i], " &", sep="")	
	abs_cast$his[i]<- paste0( "\\MyIndent ", abs_cast$his[i], sep="")
}

write.table(abs_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/abs.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

chs <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_chs.csv", header=TRUE)

chs$overall <- format(chs$overall, digits=1, nsmall=1)

chs_cast <- cast(chs, chs~alc)



chs_cast <- rename(chs_cast,c("chs"="his","1"="One","0"="Zero"))

attach(chs_cast)
chs_cast$his[his ==0] <- "0"
chs_cast$his[his ==1] <- "1-2"
chs_cast$his[his ==2] <- "3+"
detach(chs_cast)



chs_cast[] <- lapply(chs_cast, as.character)

for (i in 1:nrow(chs_cast)) {
	chs_cast$Zero[i]<- paste(chs_cast$Zero[i], " &", sep="")
	chs_cast$One[i]<- paste(chs_cast$One[i], " \\\\", sep="")	
	chs_cast$his[i]<- paste(chs_cast$his[i], " &", sep="")	
	chs_cast$his[i]<- paste0( "\\MyIndent ", chs_cast$his[i], sep="")
}

write.table(chs_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/chs.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)

soc <- read.csv("~/Dropbox/Papers/ALCAlg/Paper/data/20160628_soc.csv", header=TRUE)

soc$overall <- format(soc$overall, digits=1, nsmall=1)

soc_cast <- cast(soc, soc~alc)



soc_cast <- rename(soc_cast,c("soc"="his","1"="One","0"="Zero"))

attach(soc_cast)
soc_cast$his[his ==0] <- "0-1"
soc_cast$his[his ==1] <- "2-4"
soc_cast$his[his ==2] <- "5-6"
detach(soc_cast)



soc_cast[] <- lapply(soc_cast, as.character)

for (i in 1:nrow(soc_cast)) {
	soc_cast$Zero[i]<- paste(soc_cast$Zero[i], " &", sep="")
	soc_cast$One[i]<- paste(soc_cast$One[i], " \\\\", sep="")	
	soc_cast$his[i]<- paste(soc_cast$his[i], " &", sep="")	
	soc_cast$his[i]<- paste0( "\\MyIndent ", soc_cast$his[i], sep="")
}

write.table(soc_cast, file="~/Dropbox/Papers/ALCAlg/Paper/data/soc.csv", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)























