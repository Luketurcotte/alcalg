##Introduction
"Delayed discharges may be regarded as an indicator of inefficient use of hospital beds." \cite{Lenzi:2014}

"Elderly patients with comorbidities and complex healthcare needs are indeed more prone to receive long-term care, and therefore more likely to suffer a delay in being discharged from hospital. \cite{Lenzi:2014}

"The acute hospital sector represents only one element of a comprehensive system of care, which also includes primary care, specialist secondary care, long-stay care (nursing homes and residential homes), and social care such as domiciliary services. For older people, who often present a complex pattern of health and social care needs, an in-patient hospital admission may only be one aspect of their care." \cite{Victor:2000}

"A major explanation for the delay in discharging older people relates to organisational factors. Indeed, Victor (1990) suggests that 80% of delay results from organisational factors." \cite{Victor:2000}

"Above all, delayed discharges are a reflection of health sys- tem quality." \cite{Costa:2012}

"The ALC construct is also used to identify delayed discharges in some jurisdictions within the Uni- ted States [5,21,22]." \cite{Costa:2012}

"In Canada, the distribu- tion of bed days among ALC patients is positively skewed [28]. This suggests that the majority of ALC patients experience short delays stemming from ineffi- ciencies, while a minority of patients experience long delays due to inadequate resources elsewhere in the health care system. " \cite{Costa:2012}

"Although rehabilitation is an important component of the hospital system, there has been relatively little study of the barriers for acute hospital patients waiting for an inpatient rehabilitation bed,8–10 " \cite{New:2014}

"As well as wasting limited health-care resources that could be better utilized, when patients are in hospital for longer than necessary each additional day of hospitalisation is reported to increase by 6% the risk of iatrogenic complications, such as medication errors, nosocomial infections and falls.19" \cite{New:2014}

"By the time the present study was commissioned, previous research had identified that, whilst elderly people, those without family carers, those living alone and those with complex medical needs were most vulnerable to delay, it was organisations, not people, that were responsible for delays (Victor et al. 2000, House of Commons Select Committee on Health 2002, National Audit Office 2003, Glasby 2004, Scottish Executive 2004). According to National Health Service (NHS) performance data for 2002: one-fifth of patients who were delayed were awaiting assessment for future care (from either health or social services); over one-third were waiting for social services; one-tenth were waiting for further NHS care; one-fifth were waiting for decisions regarding funding to be made; and just under one-tenth of patients were ‘patient choice’ delays (House of Com- mons Select Committee on Health 2002, part 1, para. 9)." \cite{Landerio:2015}

"This literature suggests that, apart from the small minority of ‘patient choice’ delays, there are three fundamental themes within which delay explanations cluster: health and social care capacity issues; internal hospital inefficiencies; and inter-agency issues." \cite{Landerio:2015}

"Both studies also high- lighted a range of internal health system inefficiencies, affecting discharge timing including: limited or inefficient advanced discharge planning; delays in making health assessments (because of shortages in occupational therapists and physiotherapists, competing priorities for time, and poor coordination and communication systems); delays in preparation of discharge medicines; and poor availability of transport services. In relation to inter-agency factors, the House of Commons Select Committee on Health (2002) observed that there was a lack of clear multi-agency discharge protocols. Payne et al. (2002) found that nurses lacked awareness of the roles, responsibilities and requirements of other agencies regarding discharge, and as a result, discharge informa- tion was poor quality and often communicated late. \cite{Landerio:2015}

"It recommended early notification of other agencies for assessments (so that there would be reasonable time for staff to assess need, plan care, involve patients and arrange care packages), and emphasised the need for strategic work to map and analyse care" \cite{Landerio:2015}




###Literature Review
####Causes
- Patient characteristics
	- Male Gender \cite{Costa:2012} and \cite{New:2013cq}
	- higher age \cite{Lenzi:2014}
		- \cite{Challis:2013uq} did not find an association
		- \cite{Costa:2012} costa found that young patients aged less than 65 had longer ALC lenghts of stay 
			- Also bolstered by \cite{New:2013cq}
		- \cite{New:2014} found that older patients had greater odds of discharge barrier
	- provision of intensive care \cite{Lenzi:2014}
	- Diagnoses
		- Stroke \cite{Costa:2012}, \cite{New:2013cq}
		- Dementia \cite{Lenzi:2014}
		- tumors \cite{Lenzi:2014}
		- femoral/shoulder fractures \cite{Lenzi:2014} 
			- Lower limb factures (commonly delayed when non-weight bearing) \cite{New:2013cq}
		- 1+ comorbidity \cite{Lenzi:2014}
		- Psychiatric diagnosis \cite{Costa:2012}
		- Traumatic brain injury \cite{New:2013cq}
	- Functional impairment (More FIM impairement = greater odds of delayed discharge) \cite{Tan:2010by}
		- Seconded by \cite{New:2014} and \cite{New:2013cq}
	- Mobility issues \cite{Worthington:2006sf}
	- Cognitive impairment \cite{Challis:2013uq}
	- Functional impairment, but not after cognitive impairment taken into account \cite{Challis:2013uq} 
	- Previous number of admissions not associated with delayed discharge \cite{Challis:2013uq}
	- Patients with injury or mental and behavioural disorders \cite{Challis:2013uq}
	- ADL and cognitive impairment common for patients waiting for nursing home admission \cite{Costa:2012}
	- Abusive behaviours \cite{Costa:2012}

- Access
	- 23.7% access to long-term/rehabilitation unit \cite{Lenzi:2014}
	- Residential Care setting
		- 28.4% access to residential care homes \cite{Lenzi:2014}
			- Rises to 45.3% for patients in a long-term/rehabilitation unit \cite{Lenzi:2014}
		- 4.6 times more likely to experience delayed discharge if discharged to nursing home \cite{Tan:2010by}
		- Access to institutional care \cite{Victor:2000}
		- Discharge to care home \cite{Challis:2013uq}
		- ALC patients waiting for nursing home admission accounted for a substantial portion of all ALC bed days (41.5%) despite accounting for a small proportion of ALC patients (8.8%) \cite{Costa:2012}
		- Waiting for residential care: waiting for high level (nursing home) or low level (hostel or supported residential service) residential care accommodation to be available accounted for 42% of discharged and 22% of unncessary days in stroke rehab unity \cite{New:2014}
	- 14.5% access to other rehabilitation services \cite{Lenzi:2014}
	- Care by foreign domestic helper associated with greater odds of delayed discharge \cite{Tan:2010by}
		- Often manifests itself as requests for extension of stay by family members 
		- This is because it can take some time to get their work permits setup 
	- Home Care
		- Receipt of post-discharge home care \cite{Victor:2000}
		- Arrangements of home care \cite{Challis:2013uq}
	- Absence of an identified placement \cite{Worthington:2006sf}
	- Failure to obtain payment for subsequent placement \cite{Worthington:2006sf} 
	- Waiting for approval for long-term care and supported care or services \cite{New:2014}
	- Waiting for accommodations \cite{New:2013cq}

- Social
	- Caregiver
		- No caregiver/Caregiver does not accept hospital discharge = 22.6% of delayed discharges for patients in long-term/rehab unit \cite{Lenzi:2014}
		- absense of a family caregiver \cite{Victor:2000}
		- Lived alone \cite{Victor:2000}
		- \cite{Challis:2013uq} did not find this to be true
	- Social Isolation \citep{Landeiro:2015}

- Care Setting
	- Patients in long-term/rehab, internal medicine and orthopedic units significantly more likely to be delayed than compared to patients in general surgery. \cite{Lenzi:2014}
	- Waits for home modification \cite{New:2014} and \cite{New:2013cq}

- Discharge Planning
	- Standardized discharge planning processes reduces the odds of delayed discharge \cite{Tan:2010by}
	- Family deliberations in discharge planning \cite{New:2014} and \cite{New:2013cq}

##Conclusions
"These results altogether support the evidence that elderly patients are more prone to have protracted hospital stay [3,24] because they present with multimorbidity or with specific problems like cognitive impairment or orthopaedic conditions that may require rehabilitation, domiciliary services or some form of institutional care that may be not immediately available at discharge [25,26]." \cite{Lenzi:2014}

"These policies and strategies had the objective to foster care coordination for older people with complex needs and to reduce healthcare costs. It is possible that the low prevalence of DHD in the region is partially accounted by these organisational efforts, in line with a recent study [12] showing that medical staff involvement and discharge planning significantly reduced DHDs in a teaching hospital." \cite{Lenzi:2014}

"We found caregiver-related issues, which emerged as non-specific requests by families to be the biggest driver of delayed discharge. It is notable that the share of such patients was significantly higher for Team B. This underlined the importance of establishing a structured discharge planning process. The differences in the discharge process between the 2 teams include the timing of the decision-making process and the team philosophy adopted with regards to adherence to targeted discharge dates. The deployment of a dedicated liaison person by Team A as a strategy may have also helped improve the level of communication and coordination between the care team and patients’ families." \cite{Tan:2010by}

"As in other studies,27 motor FIM score at admission, which measures the extent of disability related to self-care, bowel and bladder continence, mobility and ambulation, was independently associated with delayed discharge. Caregivers of stroke patients face substantial stress28,29 and their burden tend to increase when caring for patients with greater motor impairment." \cite{Tan:2010by}

"While other studies found cognitive dysfunction to prolong LOS,33 we did not find cognitive status to be independently associated with delayed discharge. The strong correlation between motor and cognitive function (P = 0.613, P <0.001) for patients with prolonged stay could indicate that the variable FIM motor score reflected the overall well-being of the patient." \cite{Tan:2010by}

"Cognitive impairment was no longer a significant predictor after the discharge variables were taken into account." \cite{Challis:2013uq}

"In this study, age was not significantly associated with delay, as in other research (Victor et al., 2000; Brasel et al., 2002). Furthermore, patients’ gender and whether they had a carer had no effect on delayed discharge, a finding different from that of other work (Hudson et al., 1995; Victor et al., 2000; Thomas et al., 2005)." \cite{Challis:2013uq}

"Responsibility for the delay appeared to be primarily attributed to the responsibilities of local authority, the organisation with principal responsibility for the coordination of care for older people on discharge from hospital. Specifically, care home admission and the provision of home care were significant predictors of both delayed discharge and increased LOS, the latter finding complementing earlier research, which demonstrated that home care can reduce hospital days (Hughes et al., 1997)." \cite{Challis:2013uq}

"A comprehensive study that explored Canad- ian ALC patients waiting for nursing home admission found that some of these patients could be discharged to a community setting with the support of transitional programs and increased community care." \cite{Costa:2012}

"We believe the reason that younger patients are more likely to have a discharge barrier is because older patients have more access to services and care options in the community (eg, the Transition Care Program and high-level care)." \cite{New:2013cq}






	
