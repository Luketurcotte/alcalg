\documentclass[11pt]{article}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\usepackage[authoryear,round,longnamesfirst]{natbib}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
%\usepackage{microtype}
\usepackage{todonotes}
\usepackage[nolist,nohyperlinks]{acronym}
\usepackage{parskip}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{setspace}
%\usepackage{indentfirst}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{booktabs}
\usepackage{fullpage}
\usepackage{pdflscape}
\setlength{\parindent}{0cm}
\usepackage[section,subsection,subsubsection]{extraplaceins}
\newcommand*{\MyIndent}{\hspace*{0.5cm}}
\setlength{\tabulinesep}{5pt}
\usepackage{float}
\usepackage[small]{titlesec}
%\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\usepackage{enumitem}
%\usepackage{palatino}
\hyphenpenalty=1000
\exhyphenpenalty=1000
\usepackage[section,subsection,subsubsection]{extraplaceins}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{lscape}
\usepackage{rotating}
\usepackage{epstopdf}
\usepackage{geometry}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}



\section{Purpose}

The purpose of this exercise is to create a new algorithm that can be used to predict which patients will be designated long-stay Alternate Level of Care (ALC) patients. Long-stay ALC patients are those with more than 30 ALC days during the episode of care. ALC patient days are an inefficient use of hospital resources. Approximately 20\% of CCC beds are occupied by an ALC patient.

This algorithm will be based on the CCC admission assessment. 

\section{Work to Date}

To date, we have created multivariate logistic regression models to estimate the likelihood of becoming a long-stay ALC patient. These models indicate the functional impairment, cognitive impairment, ADRD, stroke, and a previous hospitalization in a CCC facility are risk factors for long-stay ALC designation. Though many believe that aggressive behaviour is an important risk factor, after accounting for cognitive impairment, the effect is modest. They are several protective factors, including clinical instability (CHESS), advanced medical treatments such as parenteral nutrition and suctioning, and hospice care. A complete model is presented in~\ref{fig:first}.


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[H]
\includegraphics[width=\maxwidth]{figure/first-1} \caption[Multivariate Logistic Regression Model Predicting Long-Stay ALC in CCC]{Multivariate Logistic Regression Model Predicting Long-Stay ALC in CCC.}\label{fig:first}
\end{figure}


\end{knitrout}

\section{Methods}

\subsection{Data Source and Event of Interest }

A linked dataset of CCRS admission assessments and WTIS patient records from January 1st, 2011 to March 31st, 2013 was used.

The event of interest is long-stay ALC patient designation, defined as 30 or more ALC days over an episode of care in CCC. ALC days do not need to be consecutive. 90\% of patients are designated ALC within 91 days of admission to a CCC facility. Among long-stay ALC patients, 90\% are first designated within 100 days of admission. Given that median time to long-stay ALC designation is 1 day after admission to CCC, the event of interest has been redefined as long-stay ALC patient designation within 105 days of admission to CCC for this work. 

The dataset includes 32,463 individual episodes of care. In total, patients in 2884 (8.9\%) episodes of care experienced the event of interest.

\subsection{Classification and Regression Tree (CART) Analysis}

Three distinct decision trees were created to group patients by risk of becoming a long-stay ALC patient. Given that we are classifying patients by a binary outcome, mean score for each group represents the proportion of individuals in the group that become a long-stay ALC patient.

Four different decision trees were created using distinct approaches:
\begin{itemize}
	\item Tree 1: Emphasis on selecting the strongest classifier that made clinical sense. When both a scale and a several related items were strong candidates, the scale was selected as the classifier. When a scale and a single related items was presented as a strong candidate, the single item chosen as the classifier.

	\item Tree 2: This tress is very similar to Tree 1, but I experimented with collapsing most of the advanced medical treatments in Section P into a single binary variable. This was done to alleviate the need for multiple consecutive splits for advanced medical treatments in Tree 1 (i.e., P1AI followed by P1AC). Also, when selecting classifiers for Tree 1, several advanced medical treatments were candidates at the same point in the tree.

	\item Tree 2: Based on the notion that aggressive behaviour is the most important predictor, this tree was based on the ABS. 

	\item Tree 3: I call this my "frugal" tree. Tree is based on the fewest number of items possible. This means scales were prioritized over items, items that were already part of the tree were prioritized over items that were marginally better classifiers, and classifiers that only separated out a small number of patients were used reluctantly (i.e., a split that only separated out 100 patients from a pool of 2000 patients). 
\end{itemize} 

\subsection{Collapsing Nodes into Risk Groups}

Nodes were grouped into six levels of risk. 

\begin{table}[H]
	\centering
	\begin{tabular}{ll}
		Risk Level & Percent of Sample \\
		1          & 30\%              \\
		2          & 25\%              \\
		3          & 15\%              \\
		4          & 15\%              \\
		5          & 10\%              \\
		6          & 5\%              
	\end{tabular}
\end{table}

Because of ties, the percent of the sample classified into each risk level may vary by tree. 

\subsection{Strong Variables Not Considered For Classification}

\begin{itemize} 
	\item P1AR - Community Skills Training
	\item P1AD - Intake/Output Monitoring
	\item P1AO - Hospice Care
	\item Q1C - Stay projected to be of short duration
	\item M5C - Pressure relieving device for bed
	\item N1A,B,C - Time Awake
	\item SLP, OT, PT, RT, Psych and Rec therapy time
\end{itemize} 

\section{Results}

\subsection{Tree 1}

C Statistic = 0.752

Zoom to read diagram.

\begin{figure*}[!th]
		\centering
		 \includegraphics[width=\textwidth]{tree1.png}
\end{figure*}

\begin{figure*}[!th]
		\centering
		 \includegraphics[width=0.5\textwidth]{tree1a.png}
\end{figure*}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[H]
\includegraphics[width=\maxwidth]{figure/second-1} \caption[Tree 1 Odds of Becoming Long-Stay ALC]{Tree 1 Odds of Becoming Long-Stay ALC.}\label{fig:second}
\end{figure}


\end{knitrout}

\subsection{Tree 2}

C Statistic= 0.748

\begin{figure*}[!th]
	\centering
	\includegraphics[width=\textwidth]{tree2.png}
\end{figure*}

\begin{figure*}[!th]
	\centering
	\includegraphics[width=0.5\textwidth]{tree2a.png}
\end{figure*}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[H]
\includegraphics[width=\maxwidth]{figure/Third-1} \caption[Tree 2 Odds of Becoming Long-Stay ALC]{Tree 2 Odds of Becoming Long-Stay ALC.}\label{fig:Third}
\end{figure}


\end{knitrout}

\subsection{Tree 3}

C Statistic= 0.743

\begin{figure*}[!th]
	\centering
	\includegraphics[width=\textwidth]{tree3.png}
\end{figure*}

\begin{figure*}[!th]
	\centering
	\includegraphics[width=0.5\textwidth]{tree3a.png}
\end{figure*}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[H]
\includegraphics[width=\maxwidth]{figure/Fourth-1} \caption[Tree 3 Odds of Becoming Long-Stay ALC]{Tree 3 Odds of Becoming Long-Stay ALC.}\label{fig:Fourth}
\end{figure}


\end{knitrout}

\subsection{Tree 4}

C Statistic= 0.716

\begin{figure*}[!th]
	\centering
	\includegraphics[width=\textwidth]{tree4.png}
\end{figure*}

Note: This tree has few terminal groups, so it's not possible to differentiate patients into two
group at the bottom end of the risk distribution. Instead of 6 risk groups, this tree has 5. Ignore the awkward risk group numbering. 

\begin{figure*}[!th]
	\centering
	\includegraphics[width=0.5\textwidth]{tree4b.png}
\end{figure*}

\begin{figure*}[!th]
	\centering
	\includegraphics[width=0.5\textwidth]{tree4a.png}
\end{figure*}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[H]
\includegraphics[width=\maxwidth]{figure/Fifth-1} \caption[Tree 4 Odds of Becoming Long-Stay ALC]{Tree 4 Odds of Becoming Long-Stay ALC.}\label{fig:Fifth}
\end{figure}


\end{knitrout}























\end{document}
