<<ADL, echo=FALSE, message=FALSE, warning=FALSE, results='hide', fig.align='center', fig.width=5, fig.height=3.5, fig.show='hold'>>=



ggplot() +
geom_bar(data=subset(data, varname == "adl"), aes(x=as.factor(paddrs), y=ColPercent, group=as.factor(level), fill=as.factor(level)), color="black", stat="identity", width=0.75)  +
xlab("PADDRS Score") +
ylab("% of Patients") +
ylim(0,101) +
theme(legend.position="bottom", legend.key = element_rect(colour = "black")) +
guides(fill = guide_legend(override.aes = list(colour = NULL))) +
scale_fill_viridis(discrete=TRUE, name="ADL-H Scale Score", labels=c("0", "1-2", "3-4", "5-6"))
@
