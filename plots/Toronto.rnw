<<Toronto, echo=FALSE, message=FALSE, warning=FALSE, results='hide', fig.align='center', fig.width=5, fig.height=3.5, fig.show='hold'>>=


library(reshape)
destin<- read.csv("../20160512_TorontoAdjusted.csv", header=TRUE)

mdestin <- melt(destin, id=c("FACILITY_CODE"))
mdestiny <- subset(mdestin, mdestin$variable !="numerator" & mdestin$variable != "denominator")
mdestiny <- subset(mdestiny, mdestiny$FACILITY_CODE !=53855 & mdestiny$FACILITY_CODE !=53949)
mdestiny$variable <- factor(mdestiny$variable, levels=c("rawrate","adjusted_qi"))
mdestiny$FACILITY_CODE <- factor(mdestiny$FACILITY_CODE, levels=c(53949,51315,51437,54742,51469,51356,53855,51441,54341,52933))



ggplot() +
geom_bar(data=mdestiny, aes(x=as.factor(FACILITY_CODE), y=value, group=variable, fill=variable),color="black", stat="identity", position="dodge")  +
xlab("Facility Ordered by Ascending Mean PADDRS Score") +
ylab("% of Admissions\n Long-Stay ALC")+ 
theme(legend.position="bottom", legend.key = element_rect(colour = "black")) +
guides(fill = guide_legend(override.aes = list(colour = NULL))) + 
scale_fill_viridis(discrete=TRUE, name="ALC Rate", labels=c("Raw","Adjusted")) + 
theme(axis.text.x = element_text(angle = 45, hjust = 1))

@

