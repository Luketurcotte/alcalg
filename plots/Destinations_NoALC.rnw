<<Destinations_NoALC, echo=FALSE, message=FALSE, warning=FALSE, results='hide', fig.align='center', fig.width=5, fig.height=3.5, fig.show='hold'>>=

destin<- read.csv("../20160512_DischargeLocation.csv", header=TRUE)


attach(destin)
destin$setting[location=="community"] <- "Community"
destin$setting[location=="died_in_facility"] <- "Death"
destin$setting[location=="hospital"] <- "Acute Care"
destin$setting[location=="other"] <- "Other"
destin$setting[location=="residential_care"] <- "Residential Care"
destin$setting[location=="rehab"] <- "Rehabilitation"
detach(destin)

destin$settings <- factor(destin$setting, levels=c("Residential Care", "Community","Rehabilitation", "Acute Care", "Death","Other"),ordered = TRUE)

ggplot() +
geom_bar(data=destin, aes(x=as.factor(paddrs), y=PCT_ROW, group=settings, fill=settings, order=settings), color="black", stat="identity",width=0.75,)  +
xlab("PADDRS Score") +
ylab("% of Patients") +
ylim(0,101) +
theme(legend.position="bottom", legend.key = element_rect(colour = "black")) + 
guides(fill = guide_legend(override.aes = list(colour = NULL))) + 
scale_fill_viridis(discrete=TRUE, name="Discharge\nDestination", labels=c("LTCF","Community","Rehabilitation","Acute Care","Death","Other")) +
guides(fill=guide_legend(nrow=2,byrow=TRUE))


@
